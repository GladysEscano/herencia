/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helpers;

/**
 *
 * @author Gladys Escaño
 */
public class Circulo extends Formas {
    
    public Circulo(){
        setDibujar("CIRCULO");       
        int c=10;
        double pi=3.14;
        double ra;
        double denom;       
        denom=2*pi;    
        ra=c/denom;        
        setEstablecerColor("" + ra);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helpers;

/**
 *
 * @author Gladys Escaño
 */
public class Cuadrado extends Formas {
    
    public Cuadrado(){
        setDibujar("CUADRADO");       
        int lad1=7;
        int lad2=7;
        double area;        
        area = lad1 * lad2;        
        setEstablecerColor("" + area);
    }    
}

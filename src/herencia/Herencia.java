/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

import Helpers.Circulo;
import Helpers.Cuadrado;
import Helpers.Linea;
import Helpers.Triangulo;

/**
 *
 * @author Gladys Escaño
 */
public class Herencia {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Circulo circulo = new Circulo();
        Linea linea = new Linea();
        Triangulo triangulo = new Triangulo();
        Cuadrado cuadrado = new Cuadrado();
        
        System.out.println("Forma 1:");
        circulo.Informacion();
        
        System.out.println("Forma 2:");
        linea.Informacion();
        
        System.out.println("Forma 3:");
        triangulo.Informacion();
        
        System.out.println("Forma 4:");
        cuadrado.Informacion();
    }   
}
